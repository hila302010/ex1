#include "queue.h"

/*
Function create dynamic array according to maxsize
input - the struct and the max size of elements
output - None.
*/
void initQueue(queue* q, unsigned int size)
{
	q->elements = new unsigned int[size];
	q->max_size = size;
	q->count = 0;
}
/*
Function insert element to the top of the queue
input - the struct and the new value to insert
output - None.
*/
void enqueue(queue* q, unsigned int newValue)
{
	unsigned int* elements = new unsigned int[q->max_size];
	if (q->count <= q->max_size-1)
	{
		elements[0] = newValue;
		for (int i = 1; i <= q->count; i++) // creating temparary array
		{
			elements[i] = q->elements[i-1];
		}
		for (int i = 0; i < q->count + 1; i++) // copying the temp array to the queue
		{
			q->elements[i] = elements[i];
		}
		q->count++;
	}
	delete[] elements;
}
/*
Function remove element from the top of the stack
input - the satck
output - return element in top of queue, or -1 if empty
*/
int dequeue(queue* q)
{
	unsigned int* elements = new unsigned int[q->max_size];
	if (q->count > 0)
	{
		return q->elements[0]; // returning the deleted argument 
		for (int i = 0; i < q->count - 1; i++) // creating temparary array
		{
			elements[i] = q->elements[i + 1];
		}
		for (int i = 0; i < q->count - 1; i++) // copying the temp array to the queue
		{
			q->elements[i] = elements[i];
		}
	}
	else
	{
		return -1;// empty list
	}
	delete[] elements;
}
/*
Function free the memory of the array
input - the struct
output - None.
*/
void cleanQueue(queue* q)
{
	delete[](q->elements); // free the memory
}

/*
Function print the line
input - the struct to print
output - None.
*/
void printQueue(queue* q)
{
	int i = 0;
	while (i <= q->count - 1)
	{
		std::cout << "argument " << i << " -> " <<q->elements[i] << std::endl;
		i++;
	}
}