#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>

/* a queue contains positive integer values. */
typedef struct queue
{
	unsigned int* elements;
	unsigned int max_size;
	unsigned int count;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);
void enqueue(queue* q, unsigned int newValue);
void printQueue(queue* q);
int dequeue(queue* q);

#endif /* QUEUE_H */
