#include "linkedList.h"

/*
Function add new number to the top of the list
input - the list and the new value to add
output - None.
*/
void insert(LIST *list, numNode* newNode)
{
	if (list->head == NULL) // head is empty
	{
		list->head = newNode; // head is the new node
	}
	else
	{
		newNode->next = list->head; // putting the new node as the head
		list->head = newNode;
	}
}

/*
Function removes first element from the list
input - the list
output - None.
*/
void remove(LIST *list)
{
	if (list->head != NULL) // only if head isn't empty
	{
		list->head = list->head->next;
	}
	
}

/*
Function create new node with the new number
input - new number to add
output - None.
*/
numNode* initNode(unsigned int newNum)
{
	numNode *num = new numNode;
	num->number = newNum;
	num->next = NULL;
	return num;
}

