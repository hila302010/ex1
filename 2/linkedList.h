#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>

typedef struct numNode {
	unsigned int number;
	struct numNode* next;
} numNode;

typedef struct LIST {
	struct numNode* head;
};

void insert(LIST *head, numNode* newNode);
void remove(LIST *head);

numNode* initNode(unsigned int newNum);

#endif /* LINKEDLIST_H */