#include "utils.h"

/*
Function reverse an array
input - the array and the size of it
output - none
*/
void reverse(int* nums, unsigned int size)
{
	int* arr = new int[size];
	// create new array (reversed)
	for (int i = 0, j = size - 1;i < size ; i ++, j--) 
	{
		arr[i] = nums[j];
	}
	// copying new array into nums
	for (int i = 0;i < size; i++)
	{
		nums[i] = arr[i];
	}
	delete arr;
}

/*
Function indox 10 numbers from user into array and reverse it
input - none
output - array reversed
*/
int* reverse10()
{
	int* array = new int[10];
	std::cout << "Enter 10 numbers: " << std::endl;
	for (int i = 1, j = 9; i <= 10; i++, j--)
	{
		std::cout << i + 1 << " - ";
		std::cin >> array[j]; // indox number - from end of array to begining
		getchar();
	}
	return array;
	delete array;
}